/*
 * libparallel – A simple C parallel processing library
 * 
 * Copyright (c) 2012, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <stdio.h>
#include <pthread.h>
#include <ucw/lib.h>
#include <ucwplus/asorts.h>
#include <CuTest.h>
#include "parallel.h"

#define THREAD_COUNT	200


pthread_mutex_t mutex;


void thread_fun(va_list ap) {
	int* ids = va_arg(ap, int*);
	int* count = va_arg(ap, int*);
	sync_start();
		ids[(*count)++] = get_thread_id();
	sync_end();
}


void test_threads(CuTest* c) {
	int ids[THREAD_COUNT];
	int expected[THREAD_COUNT];
	int count = 0;

	for (int i = 0; i < THREAD_COUNT; i++) {
		expected[i] = i;
	}
	pthread_mutex_init(&mutex, NULL);

	run_parallel(thread_fun, THREAD_COUNT, ids, &count);

	CuAssertIntEquals_Msg(c, "thread count", THREAD_COUNT, count); 
	ints_sort(ids, THREAD_COUNT);
	CuAssertIntArray(c, ids, expected, THREAD_COUNT, "id");

	pthread_mutex_destroy(&mutex);
}


void get_default_thread_count(va_list ap) {
	int* count = va_arg(ap, int*);
	*count = get_thread_count();
}


void test_default_threads(CuTest* c) {
	int count;
	run_parallel(get_default_thread_count, 0, &count);
	CuAssertIntEquals_Msg(c, "thread count", PROCESSOR_COUNT, count);
}
