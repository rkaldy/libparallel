/*
 * libparallel – A simple C parallel processing library
 * 
 * Copyright (c) 2012, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef PARALLEL_H
#define PARALLEL_H

#include <stdarg.h>


typedef void (*thread_fun_t)(va_list);


/*
 * Get current thread ID. The thread ID is unique among threads started by run_parallel() call
 * and it's always an integer between 0 and <num_threads>.
 */
int get_thread_id();


/*
 * Get number of threads in a parallel run the current thread belongs to.
 */
int get_thread_count();


/*
 * Start synchronized section among current parallel run
 */
void sync_start();


/*
 * End synchronized section among current parallel run
 */
void sync_end();


/*
 * Run a function parallel in <num_threads> threads. Each thread is identified by ID retrieved by get_thread_id();
 *
 * @in fun			Function to be run. Takes void* as parameter and unlike pthread_create() it doesn't return anything.
 * @in num_threads	Number of threads. If zero, then creates one thread for each available CPU.
 * @in ...			Function arguments, they are passed as va_list to <fun>.
 */
void run_parallel(thread_fun_t fun, int num_threads, ...);


#endif  // PARALLEL_H
