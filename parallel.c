/*
 * libparallel – A simple C parallel processing library
 * 
 * Copyright (c) 2012, Robert Káldy <robert@antonio.cz>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include "parallel.h"


typedef struct {
	int id;
	pthread_t sys_id;
	int count;
	thread_fun_t fun;
	pthread_mutex_t* mutex;
	va_list arg;
} thread_t;


pthread_key_t thread_id_key = 0;
pthread_key_t thread_count_key = 0;
pthread_key_t thread_mutex_key = 0;


void* start_thread(void* a_thread) {
	thread_t* thread = (thread_t*)a_thread;
	pthread_setspecific(thread_id_key, (void*)(intptr_t)thread->id);
	pthread_setspecific(thread_count_key, (void*)(intptr_t)thread->count);
	pthread_setspecific(thread_mutex_key, (void*)thread->mutex);
	(*thread->fun)(thread->arg);
	return NULL;
}


int get_thread_id() {
	return (intptr_t)pthread_getspecific(thread_id_key);
}

int get_thread_count() {
	return (intptr_t)pthread_getspecific(thread_count_key);
}


void sync_start() {
	pthread_mutex_lock((pthread_mutex_t*)pthread_getspecific(thread_mutex_key));
}

void sync_end() {
	pthread_mutex_unlock((pthread_mutex_t*)pthread_getspecific(thread_mutex_key));
}


void run_parallel(thread_fun_t fun, int num_threads, ...) {
	va_list arg;
	pthread_mutex_t mutex;
	
	if (num_threads == 0) {
		num_threads = sysconf(_SC_NPROCESSORS_ONLN);
	}
	thread_t* threads = malloc(num_threads * sizeof(thread_t));

	va_start(arg, num_threads);
	pthread_mutex_init(&mutex, NULL);
	pthread_key_create(&thread_id_key, NULL);
	pthread_key_create(&thread_count_key, NULL);
	pthread_key_create(&thread_mutex_key, NULL);

	for (int i = 0; i < num_threads; i++) {
		threads[i].id = i;
		threads[i].count = num_threads;
		threads[i].fun = fun;
		threads[i].mutex = &mutex;
		va_copy(threads[i].arg, arg);
		pthread_create(&threads[i].sys_id, NULL, &start_thread, &threads[i]);
	}

	for (int i = 0; i < num_threads; i++) {
		pthread_join(threads[i].sys_id, NULL);
	}
	
	pthread_mutex_destroy(&mutex);
	free(threads);
}
