cmake_minimum_required(VERSION 2.8)
project(parallel)

enable_testing()
add_definitions(-std=c99)
add_library(parallel SHARED parallel.c)

add_subdirectory(test)

install(TARGETS parallel DESTINATION lib)
