# libparallel

Simple C library for parallel processing.

## Requirements

* libpthread
* cmake (if you want to build shared library)

## Installation

Either use the `parallel.h` and `parallel.c` files directly or build a shared library:

	cd build
	cmake ..
	make install

For unit tests, run `ctest`.

## Usage

	#include "parallel.h"
	static int counter = 0;

Launch `parallel_fun()` in 4 threads, with two parameters `"string_param"` and `42`. The parameters type and number are arbitrary. It returns when all threads have done their work.
	
	run_parallel(parallel_fun, 4, "string_param", 42);

Define parallel function as following:

	void parallel_fun(va_list ap) {
		char* string_param = va_arg(ap, char*);
		int int_param = va_arg(ap, int);

You can retrieve the actual thread id (`0..thread count-1`) in the current `run_parallel()` run.

		printf("Launched in thread #%d of %d\n", get_thread_id(), get_thread_count());

There is a default mutex shared by all threads in the current parallel run. To access a shared resource use:

		sync_start();
		counter++;
		sync_end();
	}

## License

This library is licensed under BSD license, see `LICENSE.txt`.
